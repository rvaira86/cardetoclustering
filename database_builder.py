#!/usr/bin/env python2
# -*- coding: utf-8 -*-

###################################################################
# File per la conversione di un dataset da dataset-file a .sqlite
# e salvataggio all'interno di un DataBase File.
###################################################################

import os
from geopy.distance import vincenty
from peewee import *

LAT_REF = 43.623319
LONG_REF = 13.514461

origin = (LAT_REF, LONG_REF)

###################################################################

dataset_folder = "dataset"  # Define dataset folder name
position_file = "beacons_position_cardeto"  # Define dataset file name
gps_file = "gps_cardeto"
dataset_ext = ".csv"  # Define dataset file extention

###################################################################

result_folder = "sqlite"  # Result folder name. Editing it is unnecessary.

###################################################################


PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))

sql_filepath = os.path.join(PROJECT_ROOT, result_folder, "database_cardeto.db")


filepath_position = os.path.join(PROJECT_ROOT, dataset_folder, position_file + dataset_ext)
filepath_gps = os.path.join(PROJECT_ROOT, dataset_folder, gps_file + dataset_ext)

try:
    file_obj = open(filepath_position, "rb")
except IOError:
    print("Dataset '" + position_file + dataset_ext + "' does not exist inside folder '" + dataset_folder + "'")
    exit()

#print file_obj.name

db = SqliteDatabase(sql_filepath)


class BaseModel(Model):
    class Meta:
        database = db


###################################################################
###################################################################
###################################################################
###################################################################
# MODEL

# Define the class from which to create the model (note: must extend "BaseModel")

class Position(BaseModel):
    "Posizione punti di interesse"
    id = CharField(primary_key=True)
    long2x = FloatField()
    lat2y = FloatField()


position_model = Position


class Gps(BaseModel):
    "Posizioni GPS"
    id = CharField()
    long2x = FloatField()
    lat2y = FloatField()
    time_stamp = DateTimeField()


gps_model = Gps

####################################################################
####################################################################


def file_len(filename):
    "Funzione che calcola il numero di righe del file"
    with open(filename) as f:
        index = 0
        for index, value in enumerate(f):
            pass
        return index+1


def build_position(record):
    "Build function for poi position"

    return position_model(id=record[0], long2x=record[1], lat2y=record[2])

def build_gps(record):
    "Build function for gps position"

    return gps_model(id=record[0], long2x=record[1], lat2y=record[2], time_stamp=record[3])


db.connect()

db.create_tables([position_model, gps_model], safe=True)

i = 1
with open(filepath_position, "rb") as pos_fileobj:
            for line in pos_fileobj:
                print(str(i) + "/" + str(file_len(filepath_position)))
                line_split = line.split(";")
                longdiff = (LAT_REF, line_split[2])
                latdiff = (line_split[1], LONG_REF)

                dy = vincenty(origin, latdiff).kilometers * 1000  # differenza in metri tra le latitudine
                dx = vincenty(origin, longdiff).kilometers * 1000  # differenze in metri tra le longitudini

                record = [line_split[0], dx, dy]
                #print type(record[0])
                build_position(record).save(force_insert=True)
                i = i + 1


with open(filepath_gps, "rb") as gps_fileobj:
            j = 1
            for line in gps_fileobj:
                print(str(j) + "/" + str(file_len(filepath_gps)))
                line_split = line.split(";")
                if line_split[1] != 'NA':
                    longdiff = (LAT_REF, line_split[2])
                    latdiff = (line_split[1], LONG_REF)

                    dy = vincenty(origin, latdiff).kilometers * 1000  # differenza in metri in latitudine
                    dx = vincenty(origin, longdiff).kilometers * 1000  # differenze in metri in longitudine
                    if dx < 1000 and dy < 1000:     # filtraggio latitudini e longitudini errate
                        record = [line_split[0], dx, dy, line_split[3]]
                        build_gps(record).save(force_insert=True)
                    else:
                        pass
                else:
                    pass
                j = j+1
######################################################################
######################################################################
