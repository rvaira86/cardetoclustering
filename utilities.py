import random


def gen_hex_colour_code():
    return '#'+''.join([random.choice('0123456789ABCDEF') for x in range(6)])

