from Tkinter import *
from PIL import Image, ImageTk
from peewee_model import Position, Gps
from peewee_model import Aoi



class Map(Canvas):
    def __init__(self, master, **kw):

        Canvas.__init__(self, master, **kw)

        scrollbar = Scrollbar(master)
        scrollbar.pack(side=RIGHT, fill=Y)

        self.T = Text(self, height=52, width=45, yscrollcommand=scrollbar.set)
        self.T.place(x=1250, y=30)

        self.T.config(state="disabled")

        scrollbar.config(command=self.T.yview)


    @classmethod
    def map_init(cls, map):
        """Metodo di classe per l'inizializzazione della mappa"""

        cls.controls = {

            "c1": Aoi(x_min=340, x_max=400, y_min=500, y_max=560),

            "c3": Aoi(x_min=465, x_max=525, y_min=270, y_max=330),

            "c7": Aoi(x_min=570, x_max=630, y_min=150, y_max=210),

            "c8": Aoi(x_min=330, x_max=390, y_min=16, y_max=76)

        }
        cls.scale = 1.369863014

        # la funzione Tk.PhotoImage() non lavora con file .png, per questo usiamo le funzioni Image.open() e
        # ImageTk.PhotoImage(). La prima ritorna un oggetto immagine PIL

        objectbg = Image.open("immagini/cardeto.png")
        cls.bg = ImageTk.PhotoImage(objectbg)               # manteniamo un riferimento altrimenti garbage-collected
        map.create_image(0, 0, image=cls.bg, anchor='nw')

        objectpdi = Image.open("immagini/pdi.png")          # bisogna prima creare un oggetto immagine PIL
        resizedpdi = objectpdi.resize((50, 50))             # ridimensionamento immagine
        cls.pdi = ImageTk.PhotoImage(resizedpdi)

        positions = Position.select()
        for i in range(0, len(positions)):

            # conversione da metri a pixel
            dx = positions[i].long2x * cls.scale
            dy = positions[i].lat2y * cls.scale

            # print (dx, dy)

            map.create_image(dx, dy, image=cls.pdi, anchor='center')

        # map.create_rectangle(0, 0, 50, 50, fill="", width="1m", outline="blue")

        for c in cls.controls:
            map.create_rectangle(cls.controls[c].x_min, cls.controls[c].y_min,
                                 cls.controls[c].x_max, cls.controls[c].y_max,
                                 fill="", width="1m", outline="blue")

    def show_legend(self):
        self.clear_log()
        self.log(txt=">> Legend (keys)\n\n")
        self.log(txt="1: Compute trajectories\n")
        self.log(txt="2: Draw single trajectory\n")
        self.log(txt="3: Clustering (agglomerative)\n")
        self.log(txt="4: Draw single cluster\n")
        self.log(txt="5: Clustering (spectral)\n")
        self.log(txt="6: Draw all clusters\n")
        self.log(txt="S: Sub-clustering\n")
        self.log(txt="L: Show legend\n")

    def clear_log(self):
        self.T.config(state="normal")
        self.T.delete(1.0, END)
        self.T.config(state="disabled")

    def log(self, txt):
        self.T.config(state="normal")
        self.T.insert(END, txt)
        self.T.config(state="disabled")

    def draw_trajectory(self, trajectory, color):
        "Disegna una traiettoria sulla mappa"

        xlast, ylast = None, None
        for p in trajectory.points:

            # Disegna un punto
            self.create_circle(p[0] * self.scale, p[1] * self.scale, 3, color)

            # Disegna un segmento
            if xlast is not None and ylast is not None:
                self.create_line(xlast * self.scale, ylast * self.scale, p[0] * self.scale,
                                 p[1] * self.scale, smooth=True, tags='line', arrow='last')
            xlast = p[0]
            ylast = p[1]

    def create_circle(self, x, y, r, color):
        "Disegna un cerchio sul canvas"
        return self.create_oval(x - r, y - r, x + r, y + r, fill=color, tags='oval')

    @staticmethod
    def clear_canvas(map, tag1, tag2):
        map.delete(tag1, tag2)





