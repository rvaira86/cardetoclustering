#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('TkAgg')

from Tkinter import *
from PIL import Image, ImageTk

import csv
import os, errno, shutil
import utilities
from drawing import Map
from peewee_model import Position, Gps, Aoi
from clustering import Clustering
from trajectory import Trajectory
from datetime import datetime

sub_cluster_dir_path = ''
first_clustering = True
MAX_CLUSTERS = 8  # MAX_CLUSTERS <= 10
MAX_CLUSTERS_SPECTRAL = 20
MAX_CLUSTERS_USER_DEFINED = False
THRESHOLDTIME = 80.0
TRAJLENGTH = 100
SCALE = 1.369863014

colors = {"purple": "#A020F0",
          "orange": "#FF8C00",
          "red": "#FF0000",
          "yellow": "#FFFF00",
          "green": "#228B22",
          "lime": "#7FFF00",
          "cyan": "#00FFFF",
          "blue": "#4169E1",
          "pink": "#FF69B4",
          "gray": "#2F4F4F"}

COLOR_BLACK = "#000000"

# Boundaries of control areas
controls = {
    "c1": Aoi(x_min=340/SCALE, x_max=400/SCALE, y_min=500/SCALE, y_max=560/SCALE),
    "c2": Aoi(x_min=465/SCALE, x_max=525/SCALE, y_min=270/SCALE, y_max=330/SCALE),
    "c7": Aoi(x_min=570/SCALE, x_max=630/SCALE, y_min=150/SCALE, y_max=210/SCALE),
    "c8": Aoi(x_min=330/SCALE, x_max=390/SCALE, y_min=16/SCALE, y_max=76/SCALE)
}

# List of all trajectories computed after the first clustering
trajectories_pool = []
# List of all trajectories
trajectories = []
# Index of the current trajectory to draw
trajectory_index = 0
# List of clusters
clusters = Clustering()
# Index of the current cluster to draw
cluster_index = 0
# Number of Trajectories per Cluster
ntc = []
# List of tracks
tracks = []
# Index of the current track to draw
track_index = 0
# Flag to prevent tracks from being computed again
tracks_computed = False
# List of macro-clusters
macro_clusters = {}
# Index of the current macro-clusters to draw
macro_index = 0

##########################
#   2) Drawing the map   #
##########################

# Map initialization

img_map = "immagini/cardeto.png"
im = Image.open(img_map)

dimensions = {"width": im.size[0], "height": im.size[1]}

tkmaster = Tk(className="TrajectoryCardeto")

map = Map(tkmaster, width=im.size[0]+375, height=im.size[1])
map.pack(side='top', expand=True, fill="both")

Map.map_init(map)

# Retrieve the list of devices id detected by beacons
devices = Gps.select().group_by(Gps.id)

map.show_legend()


# Function to compute trajectories
def compute_trajectories(event):
    map.delete('line', 'oval')
    map.clear_log()
    map.log(txt=">> 1: Compute trajectories\n\n")

    # trajectories --> vettore che conterra i diversi oggetti traiettoria
    global trajectory_index, ntc, trajectories, trajectories_pool

    trajectory_index = 0
    ntc = 0
    trajectories = []
    progress_device = 0

    # per ogni device
    for device in devices:

        map.log(txt="Progress:\t" + '{0:.3g}'.format(100 * (float(progress_device) / float(devices.count()))) + "%\n")
        map.update()

        # per ogni device ordina le coordinate gps da quella con time-stamp maggiore (parte quindi dall'ultima)
        # ed ottiene una lista di oggetti peewee contenente tutti i record relativi ad un determinato device
        instances = list(
        Gps.select()
            .order_by(Gps.time_stamp.desc())
            .where(Gps.long2x < 1224/SCALE).where(Gps.lat2y < 799/SCALE)   # elimina i punti al di fuori della finestra
            .where(Gps.id == device.id)
        )


        # if instances[0].id == 'ZY222XXZZ7':
        #     for punto in instances:
        #         print (punto.long2x, punto.lat2y)


        # se ad un solo device è associato un solo punto -> continua con il prossimo device

        trajectory = []  # lista dei punti che formano una traiettoria
        trajectories_temp = []
        timestampOld = None

        for i in range(len(instances)):
            if i > 0:
                delta = abs(timestampOld - datetime.strptime(instances[i].time_stamp, '%Y-%m-%d %H.%M '))
                timestampOld = datetime.strptime(instances[i].time_stamp, '%Y-%m-%d %H.%M ')

                if delta.seconds > THRESHOLDTIME:

                    if len(trajectory) > 1:     # a causa di questo controllo la sommma delle lunghezze dei vettori
                                                # trajecories_temp[i] potrebbe non corrispondere a len(instances) -> le
                                                # traiettorie da un punto vengono scartate

                        trajectories_temp.append(trajectory)
                        trajectory = [instances[i]]
                    else:
                        trajectory = [instances[i]]     # inizia una nuova traiettoria con punto iniziale instances[i]

                else:

                    trajectory.append(instances[i])
                    if i == len(instances) - 1 and len(trajectory) > 1:    # se siamo a fine lista

                        trajectories_temp.append(trajectory)

            else:
                timestampOld = datetime.strptime(instances[i].time_stamp, '%Y-%m-%d %H.%M ')
                trajectory.append(instances[i])

        # print (len(trajectories_temp), len(instances), trajectories_temp[0][0].id)

        # if instances[0].id == '6D6CFD95-8F25-45AA-B35B-E84B5794966F':
        #     for punto in trajectories_temp[8]:
        #         print (punto.long2x, punto.lat2y)


        # for i in range(len(instances)):
        #     if i > 0:
        #         delta = abs(timestampOld - datetime.strptime(instances[i].time_stamp, '%Y-%m-%d %H.%M '))
        #         timestampOld = datetime.strptime(instances[i].time_stamp, '%Y-%m-%d %H.%M ')
        #
        #         if delta.seconds > THRESHOLDTIME:
        #
        #                 objTraj = Trajectory(trajectory)
        #                 objTraj.clean()
        #                 objTraj.filter()
        #
        #                 if objTraj.length() > TRAJLENGTH and len(objTraj.points) > 5:
        #                     trajectories_temp.append(trajectory)
        #                     trajectories.append(objTraj)
        #                 trajectory = [instances[i]]     #inizia una nuova traiettoria con punto iniziale instances[i]
        #         else:
        #             trajectory.append(instances[i])
        #             if i == len(instances) - 1:         #se siamo a fine lista delle istanze chiude la traiettoria
        #                 objTraj = Trajectory(trajectory)
        #                 objTraj.clean()
        #                 objTraj.filter()
        #                 if objTraj.length() > TRAJLENGTH and len(objTraj.points) > 5:
        #                     trajectories_temp.append(trajectory)
        #                     trajectories.append(objTraj)
        #     else:
        #         timestampOld = datetime.strptime(instances[i].time_stamp, '%Y-%m-%d %H.%M ')
        #         trajectory.append(instances[i])

        # quante_volte_dentro = 0

        for i in range(len(trajectories_temp)):

            current_index = 0
            begin_index = 0
            last_aoi = None
            first_point = True   # <- flag che indica se stiamo considerando il primo punto

            for instance in trajectories_temp[i]:

                isinside = instance.multinside(controls)

                # se mi trovo al di fuori di un punto di controllo
                if not isinside[0]:
                    first_point = False
                    if current_index == len(trajectories_temp[i]) - 1:
                        run = trajectories_temp[i][begin_index:current_index+1]
                        sottotraiettoria = Trajectory(run)

                        #filtraggio
                        sottotraiettoria.clean()
                        sottotraiettoria.filter()

                        if sottotraiettoria.length() > 100 and len(sottotraiettoria.getPoints()) > 6:
                            trajectories.append(sottotraiettoria)

                else:
                    # se all'interno di un punto di controllo
                    if isinside[1] == last_aoi:
                        first_point = False
                        if current_index == len(trajectories_temp[i]) - 1:
                            run = trajectories_temp[i][begin_index:current_index+1]
                            sottotraiettoria = Trajectory(run)

                            # filtraggio
                            sottotraiettoria.clean()
                            sottotraiettoria.filter()

                            if sottotraiettoria.length() > 100 and len(sottotraiettoria.getPoints()) > 6:
                                trajectories.append(sottotraiettoria)

                    else:
                        if first_point:
                            # la traiettoria inizia all'interno di un punto di controllo
                            first_point = False
                            last_aoi = isinside[1]

                        else:
                            # se mi trovo in un'area di controllo diversa e la traiettoria non e appena iniziata vuol dire
                            # che sono arrivato in una nuova area di controllo e quindi devo spezzare la traiettoria
                            run = trajectories_temp[i][begin_index:current_index+1]
                            last_aoi = isinside[1]
                            first_point = True
                            begin_index = current_index + 1
                            sottotraiettoria = Trajectory(run)

                            # filtraggio
                            sottotraiettoria.clean()
                            sottotraiettoria.filter()

                            if sottotraiettoria.length() > 100 and len(sottotraiettoria.getPoints()) > 6:
                                trajectories.append(sottotraiettoria)

                current_index += 1

        progress_device += 1        # progresso della computazione sul log

    #trova la lunghezza minima della traiettoria nel vettore traiettorie
    # min = 0
    # for counter, element in enumerate(trajectories):
    #     if counter == 0:
    #         min = element.length()
    #         continue
    #     if element.length() < min:
    #         min = element.length()
    #     # print (element.length())
    #
    # print ("valore minimo %f" % (min))

    # #stampa lunghezza traiettoria e numero di punti da cui è composta
    #
    # for counter, element in enumerate(trajectories):
    #     print ("La traiettoria %d e lunga %f metri ed e costituita da %d punti" % (counter, element.length(),
    #                                                                                len(element.getPoints())))

    # print "Punti traiettoria: ", trajectories[4].points
    # print "prefixSum: ", trajectories[4].prefixSum
    # print "distances: ", trajectories[4].distances
    # for i in range(len(trajectories[4].distances)):
    #     if trajectories[4].distances[i] == 0:
    #         print "stesso punto"
    #
    # print("##############################################################################################")
    #
    # print ("Lunghezza vettore temporaneo: ", len(trajectories_temp))
    #
    # for i in range(len(trajectories)):
    #     if trajectories[i].deviceId == "9E77BA29-B971-48D1-9075-9BF76FAA7263":
    #         print trajectories[i].distances

    # uscita ciclo for iniziale
    # print (len(trajectories_temp))
    #print (len(trajectories))


    map.log(txt="Progress: 100%\n\n")
    map.log(txt="Number of trajectories:\t" + str(len(trajectories)) + "\n")
    map.log(txt="\nComputing tracks..\n")


    # contatore_subtraj = 0
    # for subtraj in trajectories:
    #     if subtraj[0][0].id == '6D6CFD95-8F25-45AA-B35B-E84B5794966F':
    #         contatore_subtraj += 1
    # print (contatore_subtraj)

    # isoliamo solo le sottotraiettoria con id 6D6CFD95-8F25-45AA-B35B-E84B5794966F e ne vediamo la lunghezza uno per uno

    # for subtraj in trajectories:
    #     print subtraj

    if first_clustering:
        trajectories_pool = trajectories

    trajectory_index = len(trajectories) - 1


def draw_single_trajectory(event):
    global trajectory_index

    Map.clear_canvas(map, 'line', 'oval')
    map.clear_log()
    map.log(txt=">> 2: Draw single trajectory\n\n")

    if len(trajectories) > 0:
        map.draw_trajectory(trajectories[trajectory_index], color="red")

        map.log(txt="Device id:\t" + str(trajectories[trajectory_index].run[0].id) + "\n")

        map.log(txt=
                "Start:\t"
                + str(trajectories[trajectory_index].run[len(trajectories[trajectory_index].run) - 1].time_stamp)
                + "\n"
                )
        map.log(txt="End:\t" + str(trajectories[trajectory_index].run[0].time_stamp) + "\n")

        map.log(txt="Trajectory index:\t" + str(trajectory_index) + "\n")

        if trajectory_index > 0:
            trajectory_index -= 1
        else:
            trajectory_index = len(trajectories) - 1


def cluster_trajectories_agglomerative(event):
    map.clear_log()
    map.log(txt=">> 3: Clustering (agglomerative)\n\n")
    Map.clear_canvas(map, 'line', 'oval')

    global cluster_index, ntc

    if len(trajectories) == 0:
        map.log(txt="Error: No trajectories computed.\n")
    else:
        map.log(txt="Clustering..\n\n")
        map.update()

        # Clustering
        clusters.clusterAgglomerative(trajectories, MAX_CLUSTERS)

        # Computes the number of trajectories per cluster
        ntc = [0] * MAX_CLUSTERS
        for t in trajectories:
            ntc[t.getClusterIdx()] += 1

        # write csv files (one file for each clusters containing all the person_id for that cluster)
        # only for the first clustering step
        if first_clustering:
            # delete all files in the csv directory
            folder = 'csv'
            for the_file in os.listdir(folder):
                file_path = os.path.join(folder, the_file)
                try:
                    if os.path.isfile(file_path):
                       os.unlink(file_path)
                    # se non è un file ma è una directory
                    elif os.path.isdir(file_path):
                         shutil.rmtree(file_path)

                except Exception as e:
                    print(e)

            for trajectory in trajectories:
                csvIndex = trajectory.getClusterIdx()
                filename = "csv/cluster%s.csv" % csvIndex

                with open(filename, "a") as file:
                     writer = csv.writer(file)
                     writer.writerow((trajectory.run[0].id,))
        else:
            for trajectory in trajectories:
                csvIndex = trajectory.getClusterIdx()
                filename = sub_cluster_dir_path + "/cluster%s.csv" % csvIndex

                with open(filename, "a") as file:
                    writer = csv.writer(file)
                    writer.writerow((trajectory.run[0].id,))




        map.log(txt="Clusters:\n")
        for i in range(MAX_CLUSTERS):
            if ntc[i] > 0:
                perc = float(ntc[i]) / float(len(trajectories)) * 100
                map.log(txt="- " + '{0:.2f}'.format(perc) + "% " + colors.keys()[i] + " (" + str(ntc[i]) + ")\n")


def sub_clustering(event):

    map.clear_log()
    map.log(txt="Insert csv name in the console\n\n")
    map.update()

    global trajectories, MAX_CLUSTERS, first_clustering, sub_cluster_dir_path

    first_clustering = False
    MAX_CLUSTERS = 5        # number of cluster to be computed during the sub-clustering process

    temp_subset = []     # sub-set of the whole trajectories
    id_subset = []       # vector that contains all the id of the starting cluster

    csv_dict = dict()
    folder = "csv"
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                with open(file_path, "r") as f:
                    reader = csv.reader(f, delimiter="\n")
                    data = list(reader)
                    row_count = len(data)
                    csv_dict.update({the_file: row_count})
        except Exception as e:
            print(e)

    print (csv_dict)
    selection = raw_input(">>Insert cluster number: ")
    cluster_number = int(selection)

    file_selected = "csv/cluster%s.csv" % selection

    try:
        if os.path.isfile(file_selected):
            with open(file_selected, "r") as f:
                reader = csv.reader(f, delimiter="\n")
                data = list(reader)

    except Exception as e:
        print(e)

    dir_path = "csv/sub_cluster%s" % selection
    sub_cluster_dir_path = dir_path

    try:
        os.makedirs(dir_path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    for count, item in enumerate(data):
        id_subset.append(item[0])

    # filtering the id_subset list to delete double entries
    seen = set()
    result = []
    for item in id_subset:
        if item not in seen:
            seen.add(item)
            result.append(item)

    id_subset = result

    # filtering original trajectories list considering only the selected id
    # trajectories will contain trajectory object as usual but only a sub set
    for id in id_subset:
        for element in trajectories_pool:
            if element.run[0].id == id and element.getClusterIdx() == cluster_number:
                temp_subset.append(element)

    trajectories = temp_subset

    map.log(txt=">>Trajectories computed\n")


def cluster_trajectories_spectral(event):
    map.clear_log()
    map.log(txt=">> 5: Clustering (spectral)\n\n")
    map.update()

    global cluster_index, ntc, g
    cluster_index = 0

    if len(trajectories) == 0:
        map.log(txt="Error: No trajectories computed.\n")
    else:
        map.log(txt="Clustering..\n\n")
        map.update()

        # Clustering
        if MAX_CLUSTERS_USER_DEFINED:
            g = clusters.clusterSpectral(trajectories, MAX_CLUSTERS_SPECTRAL)
        else:
            g = clusters.clusterSpectral(trajectories)

        for i in range(g):
            print i

        # Computes the number of trajectories per cluster
        ntc = [0] * g
        for t in trajectories:
            ntc[t.getClusterIdx()] += 1

        map.log(txt="Clusters:\n")

        if MAX_CLUSTERS_USER_DEFINED:
            for i in range(MAX_CLUSTERS_SPECTRAL):
                if ntc[i] > 0:
                    perc = float(ntc[i]) / float(len(trajectories)) * 100
                    map.log(txt="- " + '{0:.2f}'.format(perc) + "% " + " (" + str(ntc[i]) + ")\n")
                    # map.log(txt="- " + '{0:.2f}'.format(perc) + "% " + colors.keys()[i] + " (" + str(ntc[i]) + ")\n")
        else:
            for i in range(g):
                if ntc[i] > 0:
                    perc = float(ntc[i]) / float(len(trajectories)) * 100
                    map.log(txt="- " + '{0:.2f}'.format(perc) + "% " + " (" + str(ntc[i]) + ")\n")
                    # map.log(txt="- " + '{0:.2f}'.format(perc) + "% " + colors.keys()[i] + " (" + str(ntc[i]) + ")\n")


def draw_single_cluster(event):
    global cluster_index, ntc

    Map.clear_canvas(map, 'line', 'oval')
    map.clear_log()
    map.log(txt='>> 4: Draw single cluster\n\n')

    if len(trajectories) == 0:
        map.log(txt="Error: No trajectories computed.\n")
    if len(ntc) == 0:
        map.log(txt="Error: No cluster computed.\n")
    else:
        if len(ntc) == 0:
            map.log(txt="Error: No cluster computed.\n")
        else:

            # print (len(ntc))
            # for i in range(len(ntc)):
            #     print (ntc[i])

            color = utilities.gen_hex_colour_code()

            # filename = "csv/cluster%s.csv" % cluster_index
            # if os.path.isfile(filename):
            #     os.remove(filename)
            #
            #
            #
            # file = open(filename, "a")
            # try:
            #     writer = csv.writer(file)
            #     writer.writerow(('cluster_id', 'trajectory_id', 'point_coordinates'))
            #     trajectory_id = 0
            #     for trajectory in trajectories:
            #         if trajectory.getClusterIdx() == cluster_index:
            #             trajectory_id += 1
            #             for counter, point in enumerate(trajectory.points):
            #                 #print type(trajectory.points), cluster_index
            #                 if counter != 0:
            #                     #print point, cluster_index
            #
            #                     writer.writerow((cluster_index, trajectory_id, point))
            #
            #             map.draw_trajectory(trajectory, color=color)
            #             # map.draw_trajectory(trajectory, color=colors.values()[cluster_index])
            #
            # finally:
            #     file.close()
            for trajectory in trajectories:
                if trajectory.getClusterIdx() == cluster_index:
                    map.draw_trajectory(trajectory, color=color)

            perc = float(ntc[cluster_index]) / float(len(trajectories)) * 100

            map.log(txt=
                    "- " + '{0:.2f}'.format(perc) + "% " + " (" + str(ntc[cluster_index]) + ")\n"
                    # "- " + '{0:.2f}'.format(perc) + "% " + colors.keys()[cluster_index]
                    # + " (" + str(ntc[cluster_index]) + ")\n"
                    )

            # incrementa l'indice che identifica il cluster da disegnare e se arriva a fine corsa lo pone a zero in
            # modo che possa ripartire
            if cluster_index < len(ntc) - 1:
                cluster_index += 1
            else:
                cluster_index = 0


def draw_all_clusters(event):

    Map.clear_canvas(map, 'line', 'oval')
    map.clear_log()
    map.log(txt='>> 6: Draw all clusters\n\n')

    if len(trajectories) == 0:
        map.log(txt="Error: No trajectories computed.\n")
    if len(ntc) == 0:
        map.log(txt="Error: No cluster computed.\n")
    else:
        if len(ntc) == 0:
            map.log(txt="Error: No cluster computed.\n")
        else:

            color_list = []
            for i in range(1, 100):
                color_list.append(utilities.gen_hex_colour_code())
            for trajectory in trajectories:
                map.draw_trajectory(trajectory, color_list[trajectory.getClusterIdx()])
                #map.draw_trajectory(trajectory, colors.values()[trajectory.getClusterIdx()])
            for i in range(len(ntc)):
                if ntc[i] > 0:
                    perc = float(ntc[i]) / float(len(trajectories)) * 100
                    map.log(txt=
                            "- " + '{0:.2f}'.format(perc) + "% " + " (" + str(ntc[i]) + ")\n"
                            # "- " + '{0:.2f}'.format(perc) + "% "
                            # + colors.keys()[i] + " (" + str(ntc[i]) + ")\n"
                            )


# Mostra la legenda quando si preme il tasto "l"
def legend(event):
    map.show_legend()
    pass


############### BINDING TK #######################

tkmaster.bind("1", compute_trajectories)
tkmaster.bind("2", draw_single_trajectory)
tkmaster.bind("3", cluster_trajectories_agglomerative)
tkmaster.bind("4", draw_single_cluster)
tkmaster.bind("5", cluster_trajectories_spectral)
tkmaster.bind("6", draw_all_clusters)
tkmaster.bind("s", sub_clustering)
tkmaster.bind("l", legend)

##################################################

mainloop()
