from peewee import *

db = SqliteDatabase('sqlite/database_cardeto.db')


class BaseModel(Model):
    class Meta:
        database = db

class Aoi(BaseModel):
    "Regioni di interesse che costituiscono la mappa"
    id = IntegerField(primary_key=True)
    x_min = FloatField()
    x_max = FloatField()
    y_min = FloatField()
    y_max = FloatField()


class Position(BaseModel):
    "Posizione punti di interesse"
    id = CharField(primary_key=True)
    long2x = FloatField()
    lat2y = FloatField()


class Gps(BaseModel):
    "Posizioni GPS dei device"
    id = CharField()
    long2x = FloatField()
    lat2y = FloatField()
    time_stamp = DateTimeField()

    def inside(self, aoi):
        return self.long2x > aoi.x_min and self.long2x < aoi.x_max and self.lat2y > aoi.y_min and self.lat2y < aoi.y_max

    def multinside(self, aois):
        for key, aoi in aois.iteritems():
            if self.inside(aoi):
                return True, key
        return False, False

